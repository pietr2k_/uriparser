<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

use Auryn\Injector;
use Klein\Klein;
use Klein\Request;
use Klein\Response;
use OLX\UriParser\Application\Service\UriParser;

require "vendor/autoload.php";

$routing = new Klein();

$routing->respond('POST', '/parse-uri', function (Request $request, Response $response) {

    $postUri = $request->param('uri');
    if($postUri == '') return $response->code(400);

    $injector = new Injector();
    $uriParser = $injector->make(UriParser::class);
    $result = $uriParser->parse($postUri);

    if ($result->getSuccess()) {
        $response->code(200);
    } else {
        $response->code(400);
    }
    $response->body(json_encode($result->getBody()));

    return $response;
});

$routing->dispatch();
