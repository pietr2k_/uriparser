<?php

namespace test\UriParser\Domain\ValueObject;


use OLX\UriParser\Domain\ValueObject\SchemeVO;
use PHPUnit\Framework\TestCase;

class SchemeVOTest extends TestCase
{
    /**
     * @dataProvider getValidDataset
     */
    public function test_ifSchemaValidaatesCorrectly($schema)
    {
        $schemaVO = new SchemeVO($schema);
        $this->assertEquals($schema, $schemaVO->getValue());
    }

    /**
     * @dataProvider getInvalidDataset
     */
    public function test_ifSchemaFailsOnInvalidData($schema)
    {
        $this->expectException(\Exception::class);
        new SchemeVO($schema);
    }

    public function getValidDataset()
    {
        return [['http'], ['https'], ['ftp'], ['mailto'], ['file'], ['data'], ['irc'],['irc:']];
    }

    public function getInvalidDataset()
    {
        return [['Http'], ['4https'], [':ftp'], ['mailt$o'], ['@file'], ['dat@a'], ['irc,']];
    }
}
