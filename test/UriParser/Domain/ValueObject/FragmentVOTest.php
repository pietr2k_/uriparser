<?php
namespace test\UriParser\Domain\ValueObject;

use OLX\UriParser\Domain\ValueObject\FragmentVO;
use PHPUnit\Framework\TestCase;

class FragmentVOTest extends TestCase
{
    /**
     * @dataProvider getValidDataset
     */
    public function test_ifFragmentValidaatesCorrectly($fragment)
    {
        $fragmentVO = new FragmentVO($fragment);
        $this->assertEquals($fragment, $fragmentVO->getValue());
    }

    /**
     * @dataProvider getInvalidDataset
     */
    public function test_ifFragmentFailsOnInvalidData($fragment)
    {
        $this->expectException(\Exception::class);
        new FragmentVO($fragment);
    }

    public function getValidDataset()
    {
        return [[null], ['fragment'], [':@&=+$./?@&-_.!~^\'()']];
    }

    public function getInvalidDataset()
    {
        return [['#'], ['`'], ['']];
    }
}


