<?php
namespace test\UriParser\Domain\ValueObject;

use OLX\UriParser\Domain\ValueObject\QueryVO;
use PHPUnit\Framework\TestCase;

class QueryVOTest extends TestCase
{
    /**
     * @dataProvider getValidDataset
     */
    public function test_ifQueryValidaatesCorrectly($Query)
    {
        $QueryVO = new QueryVO($Query);
        $this->assertEquals($Query, $QueryVO->getValue());
    }

    /**
     * @dataProvider getInvalidDataset
     */
    public function test_ifQueryFailsOnInvalidData($Query)
    {
        $this->expectException(\Exception::class);
        new QueryVO($Query);
    }

    public function getValidDataset()
    {
        return [[null], ['Queryname=3dwe32&fdsa=32r32da'], [':@&=+$./?@&-_.!~^\'()']];
    }

    public function getInvalidDataset()
    {
        return [['#'], ['`'], ['']];
    }
}


