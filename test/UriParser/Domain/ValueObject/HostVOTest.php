<?php
namespace test\UriParser\Domain\ValueObject;

use OLX\UriParser\Domain\ValueObject\HostVO;
use PHPUnit\Framework\TestCase;

class HostVOTest extends TestCase
{
    /**
     * @dataProvider getValidDataset
     */
    public function test_ifHostValidaatesCorrectly($host)
    {
        $hostVO = new HostVO($host);
        $this->assertEquals($host, $hostVO->getValue());
    }

    /**
     * @dataProvider getInvalidDataset
     */
    public function test_ifHostFailsOnInvalidData($host)
    {
        $this->expectException(\Exception::class);
        new HostVO($host);
    }

    public function getValidDataset()
    {
        return [
            ['www.google.com'],
            ['hostname.co.uk'],
            ['ftp.is.co.za'],
            ['spinaltap.micro.umn.edu'],
            ['www.math.uio.no'],
            ['ifi.unizh.ch'],
            ['comp.infosystems.www.servers.unix'],
            ['melvyl.ucop.edu'],
        ];
    }

    public function getInvalidDataset()
    {
        return [['#'], ['http://www.math.uio.no'], ['www.'], ['melvyl.ucop.edu/']];
    }
}
