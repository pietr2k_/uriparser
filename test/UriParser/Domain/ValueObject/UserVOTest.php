<?php
namespace test\UriParser\Domain\ValueObject;

use OLX\UriParser\Domain\ValueObject\UserVO;
use PHPUnit\Framework\TestCase;

class UserVOTest extends TestCase
{
    /**
     * @dataProvider getValidDataset
     */
    public function test_ifUserValidaatesCorrectly($user)
    {
        $userVO = new UserVO($user);
        $this->assertEquals($user, $userVO->getValue());
    }

    /**
     * @dataProvider getInvalidDataset
     */
    public function test_ifUserFailsOnInvalidData($user)
    {
        $this->expectException(\Exception::class);
        new UserVO($user);
    }

    public function getValidDataset()
    {
        return [[null], ['username'], ['a-zA-Z0-9:;&=+$,-_.!~^()']];
    }

    public function getInvalidDataset()
    {
        return [[''],['#'], ['dsadsada@'], ['!sda2$^#^*%']];
    }
}
