<?php
namespace test\UriParser\Domain\ValueObject;

use OLX\UriParser\Domain\ValueObject\PortVO;
use PHPUnit\Framework\TestCase;

class PortVOTest extends TestCase
{
    /**
     * @dataProvider getValidDataset
     */
    public function test_ifPortValidaatesCorrectly($port)
    {
        $portVO = new PortVO($port);
        $this->assertEquals($port, $portVO->getValue());
    }

    /**
     * @dataProvider getInvalidDataset
     */
    public function test_ifPortFailsOnInvalidData($port)
    {
        $this->expectException(\Exception::class);
        new PortVO($port);
    }

    public function getValidDataset()
    {
        return [[null], ['80'], ['1234'], ['8080'], ['553'], ['9090']];
    }

    public function getInvalidDataset()
    {
        return [[''], ['31234'], ['adrq'], ['3245efdsf']];
    }
}
