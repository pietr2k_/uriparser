<?php

namespace test\UriParser\Domain\Entity;

use OLX\UriParser\Domain\Entity\Uri;
use OLX\UriParser\Domain\ValueObject\FragmentVO;
use OLX\UriParser\Domain\ValueObject\HostVO;
use OLX\UriParser\Domain\ValueObject\PathVO;
use OLX\UriParser\Domain\ValueObject\PortVO;
use OLX\UriParser\Domain\ValueObject\QueryVO;
use OLX\UriParser\Domain\ValueObject\SchemeVO;
use OLX\UriParser\Domain\ValueObject\UserVO;
use PHPUnit\Framework\TestCase;

class UriTest extends TestCase
{
    public function test_ifEntityExportsCorrectly()
    {
        $uri = new Uri();
        $uri->setScheme(new SchemeVO('http'))
            ->setUser(new UserVO('user:pass'))
            ->setHost(new HostVO('host.pl'))
            ->setPort(new PortVO('443'))
            ->setPath(new PathVO('list/all'))
            ->setQuery(new QueryVO('key=val'))
            ->setFragment(new FragmentVO('end'));

        $this->assertEquals([
            'scheme' => 'http',
            'user' => 'user:pass',
            'host' => 'host.pl',
            'port' => '443',
            'path' => 'list/all',
            'query' => 'key=val',
            'fragment' => 'end'
        ], $uri->exportComponentsArray());
    }
}
