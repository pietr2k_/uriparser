<?php

namespace test\UriParser\Domain\Service;

use OLX\UriParser\Domain\Builder\UriBuilder;
use OLX\UriParser\Domain\DTO\AuthorityDTO;
use OLX\UriParser\Domain\DTO\UriDTO;
use OLX\UriParser\Domain\Entity\Uri;
use OLX\UriParser\Domain\Service\BreakAuthorityIntoComponents;
use OLX\UriParser\Domain\Service\BreakUriIntoComponents;
use OLX\UriParser\Domain\Service\ParseUri;
use PHPUnit\Framework\TestCase;

class ParseUriTest extends TestCase
{
    /** @var  ParseUri */
    private $parserUri;
    private $breakUriIntoComponents;
    private $breakAuthorityIntoComponents;
    private $urlBuilder;

    public function setUp()
    {
        $this->urlBuilder = $this->prophesize(UriBuilder::class);
        $this->breakUriIntoComponents = $this->prophesize(BreakUriIntoComponents::class);
        $this->breakAuthorityIntoComponents = $this->prophesize(BreakAuthorityIntoComponents::class);

        $this->parserUri = new ParseUri(
            $this->breakUriIntoComponents->reveal(),
            $this->breakAuthorityIntoComponents->reveal(),
            $this->urlBuilder->reveal()
        );
    }

    /**
     * @dataProvider getTestData
     */
    public function test_assertThatItWorks($testUrl)
    {
        $uriDTO = new UriDTO();
        $uriDTO->setAuthority('element');
        $uriReturn = $uriDTO;

        $authorityDTO = new AuthorityDTO();
        $this->breakUriIntoComponents->execute($testUrl)->shouldBeCalled()->willReturn($uriReturn);
        $this->breakAuthorityIntoComponents->execute('element')->shouldBeCalled()->willReturn($authorityDTO);

        $this->urlBuilder->build($uriReturn, $authorityDTO)->shouldBeCalled()->willReturn(new Uri());

        $this->assertTrue(($this->parserUri->run($testUrl) instanceof Uri));
    }

    public function getTestData()
    {
        return [
            ['https://www.google.pl/list?query=dsada&something=%F3dfdasd#fragmentlast'],
            ['rdar://1234']
        ];
    }
}
