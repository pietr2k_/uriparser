<?php

namespace test\UriParser\Application\Service;

use Auryn\Injector;
use OLX\UriParser\Application\Service\UriParser;
use PHPUnit\Framework\TestCase;

class UriParserTest extends TestCase
{
    /**
     * @dataProvider getTestUrls
     */
    public function test_ifUrlWillReturnInvalid($testUrl, $isUrlValid)
    {
        $injector = new Injector();
        $uriParser = $injector->make(UriParser::class);

        $this->assertEquals($isUrlValid, $uriParser->parse($testUrl)->getSuccess());
    }

    /**
     * @dataProvider getInvalidDataset
     */
    public function test_ifUrlWillThrowException($testUrl)
    {
        $injector = new Injector();
        $uriParser = $injector->make(UriParser::class);

        $this->assertEquals(false, $uriParser->parse($testUrl)->getSuccess());

    }

    public function getTestUrls()
    {
        return [
            ['ftp://ftp.is.co.za/rfc/rfc1808.txt', true],
            ['gopher://spinaltap.micro.umn.edu/00/Weather/California/Los%20Angeles', true],
            ['http://www.math.uio.no/faq/compression-faq/part1.html', true],
            ['mailto:mduerst@ifi.unizh.ch', true],
            ['news:comp.infosystems.www.servers.unix', true],
            ['telnet://melvyl.ucop.edu/', true],
            ['https://example.org/absolute/URI/with/absolute/path/to/resource.txt', true],
            ['https://example.org/absolute/URI/with/absolute/path/to/resource', true],
            ['ftp://example.org/resource.txt', true],
            ['urn:ISSN:1535-3613', true],
            ['https://example.org/absolute/URI/with/absolute/path/to/resource.txt', true],
            ['//example.org/scheme-relative/URI/with/absolute/path/to/resource.txt' , true],
            ['//example.org/scheme-relative/URI/with/absolute/path/to/resource' , true],
            ['www.google.com', true],
            ['google.org', true],
            ['http://www.google.xx', true],
            ['https://www.google.pl', true],
            ['http://foo.com/blah_blah', true],
            ['http://foo.com/blah_blah/', true],
            ['http://foo.com/blah_blah_(wikipedia)', true],
            ['http://foo.com/blah_blah_(wikipedia)_(again)', true],
            ['http://www.example.com/wpstyle/?p=364', true],
            ['https://www.example.com/foo/?bar=baz&inga=42&quux', true],
            ['http://userid:password@example.com:8080', true],
            ['http://userid:password@example.com:8080/', true],
            ['http://userid@example.com', true],
            ['http://userid@example.com/', true],
            ['http://userid@example.com:8080', true],
            ['http://userid@example.com:8080/', true],
            ['http://userid:password@example.com', true],
            ['http://userid:password@example.com/', true],
            ['http://142.42.1.1/', true],
            ['http://142.42.1.1:8080/', true],
            ['http://foo.com/blah_(wikipedia)#cite-1', true],
            ['http://foo.com/blah_(wikipedia)_blah#cite-1', true],
            ['http://foo.com/(something)?after=parens', true],
            ['http://code.google.com/events/#&product=browser', true],
        ];
    }

    public function getInvalidDataset()
    {
        return [
            ['http:/userid@example.com', false],
            ['http\userid@example.com', false],
            ['http:"examplecompl.pl', false],
            ['http://examplecompl.pl@', false],
            ['http://examplecompl.(pl)', false],
        ];
    }

}

