<?php

namespace OLX\UriParser\Domain\Service;

use OLX\UriParser\Domain\DTO\AuthorityDTO;

class BreakAuthorityIntoComponents
{
    public function execute($authority): AuthorityDTO
    {
        $authorityDTO = new AuthorityDTO();

        if (preg_match('/@/', $authority)) {
            $authorityArray = explode('@', $authority);
            $user = $authorityArray[0];
            $authorityDTO->setUsername($user);
            $authority = $authorityArray[1];
        }

        if (preg_match('/:/', $authority)) {
            $hostArray = explode(':', $authority);
            $authorityDTO->setHost($hostArray[0]);
            $authorityDTO->setPort($hostArray[1]);
        } else {
            $authorityDTO->setHost($authority);
        }

        return $authorityDTO;
    }

}
