<?php
namespace OLX\UriParser\Domain\Service;

use OLX\UriParser\Domain\Builder\UriBuilder;

class ParseUri
{
    /**
     * @var UriBuilder
     */
    private $uriBuilder;
    /**
     * @var BreakUriIntoComponents
     */
    private $breakUriIntoComponents;
    /**
     * @var BreakAuthorityIntoComponents
     */
    private $breakAuthorityIntoComponents;

    public function __construct(
        BreakUriIntoComponents $breakUriIntoComponents,
        BreakAuthorityIntoComponents $breakAuthorityIntoComponents,
        UriBuilder $uriBuilder
    )
    {
        $this->uriBuilder = $uriBuilder;
        $this->breakUriIntoComponents = $breakUriIntoComponents;
        $this->breakAuthorityIntoComponents = $breakAuthorityIntoComponents;
    }

    public function run(string $uri)
    {
        $parsedUriDTO = $this->breakUriIntoComponents->execute($uri);
        $parsedAuthority = $this->breakAuthorityIntoComponents->execute($parsedUriDTO->getAuthority());
        return $this->uriBuilder->build($parsedUriDTO, $parsedAuthority);
    }
}
