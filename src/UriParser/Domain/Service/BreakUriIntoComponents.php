<?php

namespace OLX\UriParser\Domain\Service;

use OLX\UriParser\Domain\DTO\UriDTO;

class BreakUriIntoComponents
{
    public function execute(string $uri): UriDTO
    {
        preg_match('/^(([^:\/?#]+):)?(\/\/)?([^\/?#]*)?([^?#]*)(\?([^#]*))?(#(.*))?/', $uri, $matches);

        $uriDTO = new UriDTO();
        if(isset($matches[2]))
            $uriDTO->setScheme($matches[2]);

        if(isset($matches[4]))
            $uriDTO->setAuthority($matches[4]);

        if(isset($matches[5]))
            $uriDTO->setPath($matches[5]);

        if(isset($matches[7]))
            $uriDTO->setQuery($matches[7]);

        if(isset($matches[9]))
            $uriDTO->setFragment($matches[9]);

        return $uriDTO;
    }
}
