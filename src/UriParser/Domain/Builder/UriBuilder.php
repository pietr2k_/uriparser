<?php

namespace OLX\UriParser\Domain\Builder;

use OLX\UriParser\Domain\DTO\AuthorityDTO;
use OLX\UriParser\Domain\DTO\UriDTO;
use OLX\UriParser\Domain\Entity\Uri;
use OLX\UriParser\Domain\ValueObject\FragmentVO;
use OLX\UriParser\Domain\ValueObject\HostVO;
use OLX\UriParser\Domain\ValueObject\PathVO;
use OLX\UriParser\Domain\ValueObject\PortVO;
use OLX\UriParser\Domain\ValueObject\QueryVO;
use OLX\UriParser\Domain\ValueObject\SchemeVO;
use OLX\UriParser\Domain\ValueObject\UserVO;

class UriBuilder
{
    /**
     * @var Uri
     */
    private $uri;

    public function __construct(Uri $uri)
    {
        $this->uri = $uri;
    }

    public function build(UriDTO $uriDTO, AuthorityDTO $authorityElements)
    {
        $this->uri->setScheme(new SchemeVO($uriDTO->getScheme()))
            ->setUser(new UserVO($authorityElements->getUser()))
            ->setHost(new HostVO($authorityElements->getHost()))
            ->setPort(new PortVO($authorityElements->getPort()))
            ->setPath(new PathVO($uriDTO->getPath()))
            ->setQuery(new QueryVO($uriDTO->getQuery()))
            ->setFragment(new FragmentVO($uriDTO->getFragment()));

        return $this->uri;
    }
}
