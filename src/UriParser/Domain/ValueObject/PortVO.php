<?php

namespace OLX\UriParser\Domain\ValueObject;

class PortVO
{
    private $value;

    public function __construct($value)
    {
        if(!$this->valid($value)) {
            throw new \Exception('Invalid port value');
        }

        $this->value = $value;
    }

    private function valid($value)
    {
        return (is_null($value) || preg_match('/^([0-9]{1,4})(-([0-9]{1,4})?)?$/', $value));
    }

    public function getValue()
    {
        return $this->value;
    }

}
