<?php

namespace OLX\UriParser\Domain\ValueObject;

class SchemeVO
{
    private $value;

    public function __construct($value)
    {
        if(!$this->valid($value)) {
            throw new \Exception('Invalid scheme value');
        }

        $this->value = $value;
    }

    private function valid($value)
    {
        return (is_null($value) || preg_match('/^[a-z][a-z0-9+\.-]+:?$/', $value));
    }

    public function getValue()
    {
        return $this->value;
    }

}
