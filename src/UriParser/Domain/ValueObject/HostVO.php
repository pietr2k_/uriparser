<?php

namespace OLX\UriParser\Domain\ValueObject;

class HostVO
{
    private $value;

    public function __construct($value)
    {
        if (!$this->valid($value)) {
            throw new \Exception('Invalid host value');
        }

        $this->value = $value;
    }

    private function valid($value)
    {
        $validIpAddressRegex = "/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/";
        $validHostnameRegex = "/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/";

        return (preg_match($validIpAddressRegex, $value) || preg_match($validHostnameRegex, $value));
    }

    public function getValue()
    {
        return $this->value;
    }
}
