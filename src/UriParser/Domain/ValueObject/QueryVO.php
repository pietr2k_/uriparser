<?php

namespace OLX\UriParser\Domain\ValueObject;

class QueryVO
{
    private $value;

    public function __construct($value)
    {
        if(!$this->valid($value)) {
            throw new \Exception('Invalid query value');
        }

        $this->value = $value;
    }

    private function valid($value)
    {
        return (is_null($value) || preg_match('/^[a-zA-Z0-9:@&\=\+\;\$\.\/\?\@\&\-\_\.\!\~\^\'()]+$/', $value));
    }

    public function getValue()
    {
        return $this->value;
    }
}
