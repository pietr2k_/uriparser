<?php

namespace OLX\UriParser\Domain\Entity;

use OLX\UriParser\Domain\ValueObject\FragmentVO;
use OLX\UriParser\Domain\ValueObject\HostVO;
use OLX\UriParser\Domain\ValueObject\PathVO;
use OLX\UriParser\Domain\ValueObject\PortVO;
use OLX\UriParser\Domain\ValueObject\QueryVO;
use OLX\UriParser\Domain\ValueObject\SchemeVO;
use OLX\UriParser\Domain\ValueObject\UserVO;

class Uri
{
    /** @var  SchemeVO */
    private $scheme;
    /** @var UserVO */
    private $user;
    /** @var HostVO */
    private $host;
    /** @var  PortVO */
    private $port;
    /** @var  PathVO */
    private $path;
    /** @var  QueryVO */
    private $query;
    /** @var FragmentVO */
    private $fragment;

    public function __construct()
    {

    }

    /**
     * @return SchemeVO
     */
    public function getScheme(): SchemeVO
    {
        return $this->scheme;
    }

    /**
     * @param SchemeVO $scheme
     */
    public function setScheme(SchemeVO $scheme)
    {
        $this->scheme = $scheme;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(UserVO $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return HostVO
     */
    public function getHost(): HostVO
    {
        return $this->host;
    }

    /**
     * @param HostVO $host
     */
    public function setHost(HostVO $host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return PortVO
     */
    public function getPort(): PortVO
    {
        return $this->port;
    }

    /**
     * @param PortVO $port
     */
    public function setPort(PortVO $port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return PathVO
     */
    public function getPath(): PathVO
    {
        return $this->path;
    }

    /**
     * @param PathVO $path
     */
    public function setPath(PathVO $path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return QueryVO
     */
    public function getQuery(): QueryVO
    {
        return $this->query;
    }

    /**
     * @param QueryVO $query
     */
    public function setQuery(QueryVO $query)
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @return FragmentVO
     */
    public function getFragment(): FragmentVO
    {
        return $this->fragment;
    }

    /**
     * @param FragmentVO $fragment
     */
    public function setFragment(FragmentVO $fragment)
    {
        $this->fragment = $fragment;
        return $this;
    }

    public function exportComponentsArray()
    {
        return [
            'scheme' => $this->scheme->getValue(),
            'user' => $this->user->getValue(),
            'host' => $this->host->getValue(),
            'port' => $this->port->getValue(),
            'path' => $this->path->getValue(),
            'query' => $this->query->getValue(),
            'fragment' => $this->fragment->getValue()
        ];
    }
}
