<?php

namespace OLX\UriParser\Application\Response;

class Response
{
    private $success;
    private $body;

    public function __construct($success, $body)
    {
        $this->success = $success;
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }
}
