<?php

namespace OLX\UriParser\Application\Service;

use Exception;
use OLX\UriParser\Application\Response\Response;
use OLX\UriParser\Domain\Service\ParseUri;

class UriParser
{
    /**
     * @var ParseUri
     */
    private $parseUri;

    public function __construct(ParseUri $parseUri)
    {
        $this->parseUri = $parseUri;
    }

    public function parse(string $uri) : Response
    {
        try {
            $uriEntity = $this->parseUri->run($uri);
            return new Response(true, $uriEntity->exportComponentsArray());

        } catch (Exception $ex) {
            return new Response(false, $ex->getMessage());
        }
    }
}
